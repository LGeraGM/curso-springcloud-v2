package com.springCloudV2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudV2ApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV2ApiGatewayApplication.class, args);
	}

}
