package com.springCloudV2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudV2LimitsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV2LimitsServiceApplication.class, args);
	}

}
