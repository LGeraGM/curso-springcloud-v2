package com.springCloudV2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SpringCloudV2CurrencyConversionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV2CurrencyConversionServiceApplication.class, args);
	}

}
