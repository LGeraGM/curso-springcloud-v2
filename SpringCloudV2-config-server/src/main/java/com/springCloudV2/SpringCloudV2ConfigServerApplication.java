package com.springCloudV2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class SpringCloudV2ConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV2ConfigServerApplication.class, args);
	}

}
