package com.springCloudV2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudV2CurrencyExchangeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV2CurrencyExchangeServiceApplication.class, args);
	}

}
