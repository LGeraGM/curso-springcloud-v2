package com.springCloudV2;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyExchangeRepository extends JpaRepository<CurrencyExchange, Long>{
CurrencyExchange findByFromAndTo(String from, String to);
}
